import * as React from "react";
import {observable, makeObservable} from "mobx";

export interface RoomInfo {
	userCount: number;
	maxUsers: number;
}

export class AppContextType {
	wbClient: any;
	constructor() {}
}

export const AppContext = React.createContext<AppContextType>(undefined);
