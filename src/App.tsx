import React, { useEffect } from 'react'
import { render } from 'react-dom'
import { GlobalStyle } from './styles/GlobalStyle'
import "./App.scss";
var mainElement: HTMLDivElement;
if (document.getElementById("root") == undefined) {

  mainElement = document.createElement('div')
  mainElement.setAttribute('id', 'root')
  document.body.appendChild(mainElement);
}
mainElement = document.getElementById("root") as HTMLDivElement;

const App = () => {
  useEffect(() => {
   
  }, []) // <-- empty dependency array
  return (
    <>

    </>
  )
}
//new ThreeApp().init();
render(<App />, mainElement)
