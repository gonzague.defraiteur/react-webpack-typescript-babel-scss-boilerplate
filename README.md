# React-Webpack-Typescript-Babel-Scss-Boilerplate

Boilerplate with react, webpack, typescript, babel, scss

## Getting started

installation:

npm install

run in dev mode:

npm run dev

build:

npm run build
